#!/bin/env ruby

require "io/console"

gen = 1
memory = [[]];
finger = 0
registers = [0]*8

File.open("sandmark.umz") do |file|
  i = 0
  until file.eof?
    bytes = file.read(4).bytes
    val = bytes[0]<<24 | bytes[1]<<16 | bytes[2]<<8 | bytes[3]
    memory[0][i] = val
    i += 1
  end
end

while true
  working_platter = memory[0][finger]
  fail unless working_platter
  operation = working_platter>>28

  a = working_platter>>6 & 7
  b = working_platter>>3 & 7
  c = working_platter>>0 & 7

  case operation
  when 0
    registers[a] = registers[b] unless registers[c].zero?
  when 1
    registers[a] = memory[registers[b]][registers[c]]
  when 2
    memory[registers[a]][registers[b]] = registers[c]
  when 3
    registers[a] = (registers[b] + registers[c]) % 2**32
  when 4
    registers[a] = (registers[b] * registers[c]) % 2**32
  when 5
    fail if registers[c] == 0
    registers[a] = (registers[b] / registers[c]).to_i % 2**32 unless registers[c].zero?
  when 6
    registers[a] = ~(registers[b] & registers[c]) & 0xFFFFFFFF
  when 7
    exit 0
  when 8
    index = gen
    gen += 1
    memory[index] = [0] * registers[c]
    registers[b] = index
  when 9
    fail if registers[c] == 0
    memory[registers[c]] = []
  when 10
    fail if registers[c] > 255
    printf registers[c].chr
    STDOUT.flush
  when 11
    registers[c] = STDIN.getc.ord
  when 12
    memory[0] = memory[registers[b]].clone unless registers[b] == 0
    finger = registers[c] - 1
  when 13
    a = working_platter>>25 & 7
    value = working_platter & 33554431
    registers[a] = value
  end
  finger += 1
end
