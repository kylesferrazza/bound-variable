#!/bin/env ruby

require "io/console"

gen = 1
memory = Array(Array(UInt32)).new
memory << (Array(UInt32).new)
finger = 0
registers = Array(UInt32).new
8.times do
  registers << 0_u32
end

File.open("sandmark.umz") do |file|
  (file.size / 4).times do
    slice = Bytes.new(4)
    file.read_fully(slice)
    tuple = {slice[3], slice[2], slice[1], slice[0]}
    ptr = Pointer(UInt32).new(pointerof(tuple).address)
    val = ptr.value
    memory[0] << val
  end
end

while true
  working_platter = memory[0][finger]
  # fail unless working_platter
  operation = working_platter>>28

  a = (working_platter>>6 & 7).to_u32
  b = (working_platter>>3 & 7).to_u32
  c = (working_platter>>0 & 7).to_u32

  case operation
  when 0
    registers[a] = registers[b] unless registers[c].zero?
  when 1
    registers[a] = memory[registers[b]][registers[c]]
  when 2
    memory[registers[a]][registers[b]] = registers[c]
  when 3
    registers[a] = (registers[b] + registers[c]) % 2**32
  when 4
    registers[a] = (registers[b] * registers[c]) % 2**32
  when 5
    # fail if registers[c] == 0
    registers[a] = (registers[b] / registers[c]).to_u32 % 2**32 unless registers[c].zero?
  when 6
    registers[a] = ~(registers[b] & registers[c]) & 0xFFFFFFFF
  when 7
    exit 0
  when 8
    index = gen
    gen += 1
    memory[index] = [0_u32] * registers[c]
    registers[b] = index.to_u32
  when 9
    # fail if registers[c] == 0
    memory[registers[c]] = Array(UInt32).new
  when 10
    # fail if registers[c] > 255
    p registers[c].chr
    STDOUT.flush
  when 11
    # registers[c] = STDIN.getc.ord
    registers[c] = UInt32.new(STDIN.read_char.as(Char).ord)
  when 12
    memory[0] = memory[registers[b]].clone unless registers[b] == 0
    finger = registers[c] - 1
  when 13
    a = working_platter>>25 & 7
    value = working_platter & 33554431
    registers[a] = value
  end
  finger += 1
end
